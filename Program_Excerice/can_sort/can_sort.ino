#include <Zumo32U4.h> 
#include <Wire.h>
#define NUM_SENSORS 5

Zumo32U4ProximitySensors Pro_Sen;
Zumo32U4LCD lcd;
Zumo32U4ProximitySensors proxSensors;
Zumo32U4LineSensors lineSensors;   // Line sensors
Zumo32U4Motors motors;

uint16_t lineSensorValues[NUM_SENSORS];      //The raw values of the sensors
uint16_t maxlineSensorValues[NUM_SENSORS];   //Max value of the sensors
uint16_t minlineSensorValues[NUM_SENSORS];   //Min value of the sensors
uint16_t output[NUM_SENSORS] = { 600,0,300,0,600};                //When the values have been kalibatet, it would end up in the output_array

bool useEmitters = true;   //The sensors emittes out IR_ligth

struct LineSensorsWhite { // True if White, False if Black
  bool L;
  bool LC;
  bool C;
  bool RC;
  bool R;
};

int threshold[NUM_SENSORS] = {100, 100, 100, 100, 100}; // White threshold, white return values lower than this
LineSensorsWhite sensorsState = {0, 0, 0, 0, 0}; // populate the struct with false values ( 0 ), like sensorsState = {false,false,false,false,false};​

void setup()
{
  lineSensors.initFiveSensors();
	Serial.begin(9600);
	delay(500);
	Serial.println("setup");
  calibrateThreshold();
  Serial.println("Calibration finalized");
	belt_opret_setup();
	can_size_setup();
}

void loop()
{
	while (digitalRead(14))
		Serial.println("venter på tryk på knap A");
	while (true)
	{
		Serial.println("");
		Serial.println("");
		Serial.println("hoved loop");
		belt_opret(800);
    if (can_size(16))
		{
    
      Serial.println("big");
			moveToBigPush();
			moveToHome();
		}
		else
		{
    
      Serial.println("smal");
      motors.setSpeeds(-150, 150);//Sets motorspeed and time, making the Zumo turn 90° to the right, to make it go along the found line.
      delay(550);
      motors.setSpeeds(150, 150);//Sets motorspeed and time, making the Zumo turn 90° to the right, to make it go along the found line.
      delay(300);
      motors.setSpeeds(150, -150);//Sets motorspeed and time, making the Zumo turn 90° to the right, to make it go along the found line.
      delay(530);
      motors.setSpeeds(150, 150);//Sets motorspeed and time, making the Zumo turn 90° to the right, to make it go along the found line.
      delay(1000);
			push_small();
		}
	}
}

void belt_opret_setup()
{
	lineSensors.initFiveSensors();
  	Serial.println("belt_opret_setup");
}

void belt_opret(int white)
{
	AdjustToLine(white);//Runs AdjustToLine codeblock - Go to "void AdjustToLine" for more info.
	motors.setSpeeds(150, 150);
	delay(250);
	motors.setSpeeds(150, -150);//Sets motorspeed and time, making the Zumo turn 90° to the right, to make it go along the found line.
	delay(500);
	do
	{
		lineSensors.read(lineSensorValues, useEmitters ? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);
		FollowLine(400);//Runs FollowLine codeblock - Go to "void FollowLine" for more info.
		Serial.println("FollowLine");
		if ((lineSensorValues[0] > white) && (lineSensorValues[2] > white) && (lineSensorValues[4] > white) && false)   //In case the Zumo is going the wrong way while following the line, this will make it detect the edge and turn it around.
		{
			Serial.println("EdgeCase");
			Serial.println((String)lineSensorValues[0] + (" ") + (String)lineSensorValues[1] + (" ") + (String)lineSensorValues[2] + (" ") + (String)lineSensorValues[3] + (" ") + (String)lineSensorValues[4]);
			motors.setSpeeds (-200, -200);
			delay(350);
			motors.setSpeeds (175, -200);
			delay(900);
		}
		Serial.println((String)lineSensorValues[0] + (" ") + (String)lineSensorValues[1] + (" ") + (String)lineSensorValues[2] + (" ") + (String)lineSensorValues[3] + (" ") + (String)lineSensorValues[4]);
	} while ((lineSensorValues [0] < white && lineSensorValues [4] < white) != true);      //Uses sensorvalues to determine if "end-of-line", and stops the motors.
  
	motors.setSpeeds(100, 100);
  	delay(30);
	motors.setSpeeds(0, 0);
	Serial.println("belt_opret");
}

void can_size_setup()
{
	proxSensors.initFrontSensor();
	uint16_t myBrightnessLevels[] = {0,1,2,3,4,5,6,7,8,9,10,12,14,17,20,23}; 
	proxSensors.setBrightnessLevels(myBrightnessLevels, 16);
	Serial.println("can_size_setup");
}

int can_size(int dist) //, int dist_can)
{
  int l = 0;
  int r = 0;
  int l_max = 0;
  int r_max = 0;
  do
  {
    lineSensors.emittersOn();
    delay(10);
    proxSensors.read();
    l = proxSensors.countsFrontWithLeftLeds();
    r = proxSensors.countsFrontWithRightLeds();
    if( l > l_max)
      l_max = l;
    if(r > r_max)
      r_max = r;
      printReadingsToSerial();
  } while( !(l+2< l_max && r+2 < r_max) || r_max < 14);
  Serial.println(l_max);
  Serial.println(r_max);
  return ((r_max+l_max) >> 1) < dist;
  /*
	uint16_t lastSampleTime = 0;
	do
	{
		proxSensors.read();
		printReadingsToSerial();
    	lineSensors.emittersOn();
	} while (proxSensors.countsFrontWithLeftLeds() < dist);

	while ((int)proxSensors.countsFrontWithLeftLeds() != (int)proxSensors.countsFrontWithRightLeds())
	{
		proxSensors.read();
    	lineSensors.emittersOn();
		printReadingsToSerial();
	}
	return dist_can < (int)proxSensors.countsFrontWithLeftLeds();
 */
}

// Comment out when done testing prox-sensors
void printReadingsToSerial()
{
	static char buffer[80];
	sprintf
	(
		buffer, 
		"%d %d\n",
		proxSensors.countsFrontWithLeftLeds(),
		proxSensors.countsFrontWithRightLeds()
	);
	Serial.print(buffer);
}

void push_small()
{
  forwardToWhite(100);
  Serial.println("forwardToWhite Done");
  delay(500);
  motors.setSpeeds(-100, -100);
  delay(300);
  Serial.println("small reverse Done");
  getMeHome();
  Serial.println("getMeHome Done");
}

void readSensors(LineSensorsWhite &state) {
  // Next line reads the sensor values and store them in the array lineSensorValues , aparameter passed by reference
  lineSensors.read(lineSensorValues, useEmitters ? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);
  // In the following lines use the values of the sensors to update the struct
  sensorsState = {false, false, false, false, false};
  if ( lineSensorValues[0] < threshold[0])
    sensorsState.L = true;
  if ( lineSensorValues[1] < threshold[1])
    sensorsState.LC = true;
  if ( lineSensorValues[2] < threshold[2])
    sensorsState.C = true;
  if ( lineSensorValues[3] < threshold[3])
    sensorsState.RC = true;
  if ( lineSensorValues[4] < threshold[4])
    sensorsState.R = true;
}

void calibrateThreshold() {
  Serial.println("Calibrating          |");
  Serial.print  ("Calibrating ");
  int  newThrehold[NUM_SENSORS] = {0, 0, 0, 0, 0};
  for (int i = 0; i < 10; i++) {
    lineSensors.read(lineSensorValues, useEmitters ? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);
    for (int j = 0; j < NUM_SENSORS; j++) {
      newThrehold[j] +=  lineSensorValues[j] * 0.1;
    }
    delay(300);
    Serial.print(">");
  }
  for (int i = 0; i < NUM_SENSORS; i++) {
    threshold[i] = newThrehold[i] - 30;
  }
}

void forwardToWhite(int speed) {
  motors.setSpeeds(speed, speed);
  do
  {
    readSensors(sensorsState);
  } while ( !(sensorsState.L && sensorsState.LC && sensorsState.C && sensorsState.RC && sensorsState.R) );
  Serial.println("forwardToWhite");

  motors.setSpeeds(0, 0);
}

void getMeHome()
{
  motors.setSpeeds(-130, 150);
  Serial.println("drej mod uret");
  //delay(525); 
  delay(250);
  motors.setSpeeds(-100, -100); 
  Serial.println("kør baglæns");
  delay(2000);

  motors.setSpeeds(-150, 150);
  Serial.println("drej mod uret");
  delay(550); 

   motors.setSpeeds(100, 100);
  Serial.println("kør frem");
  delay(1000); 

  motors.setSpeeds(150, -150);
  Serial.println("drej med uret");
  delay(520); 
  motors.setSpeeds(0, 0);
}
void moveToBigPush()
{
	motors.setSpeeds(120, -20);
	delay(750);
	motors.setSpeeds(0, 0);
	delay(1200);
	motors.setSpeeds(100, 100);
	delay(2000);
	motors.setSpeeds(-100, 100);
	delay(1200);
	motors.setSpeeds(0, 0);
	delay(1000);
  // reads the lineValues 
  lineSensors.read(lineSensorValues, useEmitters ? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);
  motors.setSpeeds(100, 100); 
	while (output[0] < lineSensorValues[0] || output[2] < lineSensorValues[2] ||  output[4] < lineSensorValues[4])
	{
		lineSensors.read(lineSensorValues, useEmitters ? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);
		Serial.println( "V 1 --> " + String(lineSensorValues[0]));
		Serial.println( "V 2 --> " + String(lineSensorValues[2]));
		Serial.println( "V 3 --> " + String(lineSensorValues[4]));
		Serial.println( "O 1 --> " + String(output[0]));
		Serial.println( "O 2 --> " + String(output[2]));
		Serial.println( "O 3 --> " + String(output[4]));
    delay(5);
		// lookes for a line
	}
	motors.setSpeeds(0, 0); 
}

void moveToHome()
{
	motors.setSpeeds(-100, -100);
 
  Serial.println("1");
	delay(3000); 
	motors.setSpeeds(-120, 20); 
	Serial.println("2");
	delay(600);  
	motors.setSpeeds(100,100); 
	/*
	Serial.println("3");
	delay(2000);  
	motors.setSpeeds(-20, 120),  
	Serial.println("4");
	delay(880); 
	motors.setSpeeds(0,0); 
 */
}

void AdjustToLine(int white)//Uses linesensors to detect a line, and turns the robot to be perpendicular on the detected line. Direction is determined by which sensor is first to detect the line.
//Code runs until the robot detects a line with all 3 sensors, indicating that it has adjusted to the line.
{
	Serial.println("AdjustToLine");
	do
	{
		lineSensors.read(lineSensorValues, useEmitters ? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);
		Serial.println((String)lineSensorValues[0] + (" ") + (String)lineSensorValues[1] + (" ") + (String)lineSensorValues[2] + (" ") + (String)lineSensorValues[3] + (" ") + (String)lineSensorValues[4]);
		if(lineSensorValues[0] < white)
		{
			Serial.println("RightRotation");
			motors.setSpeeds(-200, 150);
		}
		else if (lineSensorValues[4] < white)
		{
			Serial.println("LeftRotation");
			motors.setSpeeds(150, -200);
		}
		else
		{
			Serial.println("DriveForward");
			motors.setSpeeds(150, 150);
		}
	} while ((lineSensorValues[0] < white && lineSensorValues[2] < white && lineSensorValues[4] < white) != true);
	motors.setSpeeds(0 , 0);
}

void FollowLine(int white)//Uses the central linesensor to follow along the edge of a line.
//If the sensor detects darker than threshold it turns toward the lineif it detects the line, it turns away from it.
//The robot will "zig-zag" a little, but will stay along the edge of the line.
{
	Serial.println("FollowLine");
	Serial.println(lineSensorValues[2]);
	if (lineSensorValues[2] > white)
	{
		Serial.println("FollowLineLeftSteer");
		motors.setSpeeds(0, 100);
	}
	else if (lineSensorValues[2] < white)
	{
		Serial.println("FollowLineRightSteer");
		motors.setSpeeds(100, 0);
	}
	else
	{
		Serial.println("FollowLineStraight");
		motors.setSpeeds(150, 150);
	}
}
