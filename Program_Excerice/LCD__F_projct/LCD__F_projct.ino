#include <Zumo32U4.h>
Zumo32U4LCD lcd;
Zumo32U4Buzzer buzzer;
Zumo32U4ButtonA buttonA;
Zumo32U4Encoders encoder;
Zumo32U4Motors motors;


/* Has to creat a program, that shows deffrint drive patherets shuch as foward, backwards or spin around.
    Af ther patherens has been repræsentet on the LCD, then one of the pathes has been selctet with the buttons on the robot.

   Then with the two encoders, time and speed is selltecet by turning rigth and left wheel.
   The speed and time is also dislpalyed on the LCD  sreen.
*/
bool presB = false;
bool presA = false;
int presANum = 0;
int speed_r = 0;
int speed_l = 0;
int time = 0;
void setup()
{
	Serial.begin(9600);
	pinMode(14, INPUT);
	pinMode(17, INPUT);
	welcome();
}

void loop()
{
	presB = false;
	presA = false;
	Choose_driraksen();
	choose_speed();
	driksen();
	driver();
	Serial.println(speed_r);
	Serial.println(speed_l);
	Serial.println(time);
}

// This dark place, considet of an endles while loop, where you well swel in the enndles void. Until you press the button b.
// The functons looks for the button B is press, but while it do, you can change the output on the screen with the button A.
//  The opstions are Foward = F, Backward = B,  turn rigt = R and turn left = L.
void Choose_driraksen()
{
	while (presB == false)
	{
		presB = !digitalRead(17);
		presA = !digitalRead(14);
		if (presA == true)
		{
			bip();
			presANum++;
			buttonA.waitForRelease();
		}
		// This part print the optines, and then preps the LCD, for the user choose
		lcd.gotoXY(0, 0);
		lcd.print("Options:");
		lcd.gotoXY(0, 1);
		switch (presANum)
		{
			case 0:
				lcd.print("|F| B R L ");
				break;
			case 1:
				lcd.print("F |B| R L");
				break;
			case 2:
				lcd.print("F B |R| L ");
				break;
			case 3:
				lcd.print("B R |L|   ");
				break;
			default:
				presANum = 0;
		}
	}
}

void driksen()
{
	switch(presANum)
	{
		case 0:
			speed_l = speed_r;
			break;
		case 1:
			speed_l = - speed_r;
			speed_r = - speed_r;
			break;
		case 2:
			speed_l = - speed_r;
			break;
		case 3:
			speed_l = speed_r;
			speed_r = -speed_r;
			break;
	}
}
void choose_speed()
{
	time = encoder.getCountsAndResetLeft();
	speed_r = encoder.getCountsAndResetRight();
	lcd.clear();
	while(!buttonA.isPressed())
	{
		time = encoder.getCountsLeft();
		speed_r = encoder.getCountsRight();
		if(time<0)
			time=0;
	
		if(time>1000)
			time=1000;
	
		if(speed_r>400)
    			speed_r=400;
	
		if(speed_r<-400)
			speed_r=-400;
	
		lcd.gotoXY(0, 0);
		lcd.print("time");
		lcd.gotoXY(4, 0);
		lcd.print(time);
		lcd.gotoXY(0, 1);
		lcd.print("sped");
		lcd.gotoXY(4, 1);
		lcd.print(speed_r);
	}
}
void driver()
{
	Serial.println("fff");
	Serial.println(speed_r);
	Serial.println(speed_l);
	Serial.println(time);
	motors.setSpeeds(speed_r,speed_l);
	delay(time*10);
	motors.setSpeeds(0,0);
}
// This is a greating for the user
void welcome()
{  
	lcd.print("Welcome");
	delay(200);
	lcd.clear();
} 

// The migit bip is here, would you like to annoy everyone in the same room? Then you have to get bip. only 0.99$
void bip()
{
	buzzer.playNote(NOTE_A(4), 20, 15);
	delay(30);
}
