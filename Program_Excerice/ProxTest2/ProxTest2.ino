#include <Wire.h>
#include <Zumo32U4.h>

Zumo32U4LCD lcd;
Zumo32U4ProximitySensors proxSensors;

bool proxLeftActive;
bool proxFrontActive;
bool proxRightActive;

void setup()
{
	proxSensors.initThreeSensors();
	loadCustomCharacters();
}


void loadCustomCharacters()
{
	static const char levels[] PROGMEM = 
	{
		0, 0, 0, 0, 0, 0, 0, 63, 63, 63, 63, 63, 63, 63
	};
	lcd.loadCustomCharacter(levels + 0, 0);  // 1 bar
	lcd.loadCustomCharacter(levels + 1, 1);  // 2 bars
	lcd.loadCustomCharacter(levels + 2, 2);  // 3 bars
	lcd.loadCustomCharacter(levels + 3, 3);  // 4 bars
	lcd.loadCustomCharacter(levels + 4, 4);  // 5 bars
	lcd.loadCustomCharacter(levels + 5, 5);  // 6 bars
	lcd.loadCustomCharacter(levels + 6, 6);  // 7 bars
}

void printBar(uint8_t height)
{
	if (height > 8) { height = 8; }
	const char barChars[] = {' ', 0, 1, 2, 3, 4, 5, 6, (char)255};
	lcd.print(barChars[height]);
}

// Prints a bar graph showing all the sensor readings on the LCD.
void printReadingsToLCD()
{
	// On the first line of the LCD, display proximity sensor
	// readings.
	lcd.gotoXY(0, 0);
	printBar(proxSensors.countsLeftWithLeftLeds());
	printBar(proxSensors.countsLeftWithRightLeds());
	lcd.print(' ');
	printBar(proxSensors.countsFrontWithLeftLeds());
	printBar(proxSensors.countsFrontWithRightLeds());
	lcd.print(' ');
	printBar(proxSensors.countsRightWithLeftLeds());
	printBar(proxSensors.countsRightWithRightLeds());

	// On the second line, display basic
	// readings of the sensors taken without sending IR pulses.
	lcd.gotoXY(3, 1);
	lcd.gotoXY(0,1);
	printBar(proxLeftActive);
	printBar(proxFrontActive);
	printBar(proxRightActive);
}

// Prints a line with all the sensor readings to the serial
// monitor.
void printReadingsToSerial()
{
	static char buffer[80];
	sprintf(buffer, "%d %d %d %d %d %d  %d %d %d\n",
		proxSensors.countsLeftWithLeftLeds(),
		proxSensors.countsLeftWithRightLeds(),
		proxSensors.countsFrontWithLeftLeds(),
		proxSensors.countsFrontWithRightLeds(),
		proxSensors.countsRightWithLeftLeds(),
		proxSensors.countsRightWithRightLeds(),
		proxLeftActive,
		proxFrontActive,
		proxRightActive
	);
	Serial.print(buffer);
}

void loop()
{
static uint16_t lastSampleTime = 0;

	if ((uint16_t)(millis() - lastSampleTime) >= 100)
	{
		lastSampleTime = millis();
	
		// Send IR pulses and read the proximity sensors.
		proxSensors.read();
		
		// Just read the proximity sensors without sending pulses.
		proxLeftActive = proxSensors.readBasicLeft();
		proxFrontActive = proxSensors.readBasicFront();
		proxRightActive = proxSensors.readBasicRight();
		
		// Send the results to the LCD and to the serial monitor.
		printReadingsToLCD();
		printReadingsToSerial();
	}
}
