#include <Zumo32U4.h>

Zumo32U4LCD lcd;
Zumo32U4ProximitySensors proxSensors;

// --- Setup function
void setup()
{
	// Get the proximity sensors initialized
	proxSensors.initThreeSensors();
}

// Main loop
void loop()
{
	// Read Sensors
	proxSensors.read();    
	
	int center_left_sensor = proxSensors.countsFrontWithLeftLeds();
	int center_right_sensor = proxSensors.countsFrontWithRightLeds();

	// Update the display
	lcd.gotoXY(0, 1);
	lcd.clear();
	lcd.print(center_left_sensor);
	lcd.print(" ");
	lcd.print(center_right_sensor);
}
