#include <Wire.h>
#include <Zumo32U4.h>

Zumo32U4LineSensors lineSensors;   // instance of the sensor
Zumo32U4LCD lcd;                   // LCD screen 
Zumo32U4Motors motors;             // Motors control

// This defines the count of sensors
#define NUM_SENSORS 5

// These are some global 16_bits int arrays, which is the size of the count of sensors
//**************************************************************************************
uint16_t lineSensorValues[NUM_SENSORS];      //The raw values of the sensors
uint16_t maxlineSensorValues[NUM_SENSORS];   //Max value of the sensors
uint16_t minlineSensorValues[NUM_SENSORS];   //Min value of the sensors
uint16_t output[NUM_SENSORS];                //When the values have been kalibatet, it would end up in the output_array

bool useEmitters = true;   //The sensors emittes out IR_ligth
int buttonA = 14;          // A pin_numb has been asigned to the button A


// This a kalibrat function, created as a int. Within, 3 paramteres as int 16_bits pointers are created. 
//*************************************************************************************
int kalibar(uint16_t *senser, uint16_t *min, uint16_t *max)
{
	lineSensors.read(senser);
	for(int i = 0; i < NUM_SENSORS;i++)
	{
		min[i] = senser[i];
		max[i] = senser[i];
	}
	lcd.print("tryk paa kanp for at stop");    // written on the LCD
	while (digitalRead(buttonA))               // a whiled loop that runs until you press the button A
	{
		lineSensors.read(senser);                // Sensor values are overwritten from the pointer 
		for (int i = 0; i < NUM_SENSORS; i++)    // A for-loop which sets the minimal and maximal sensor values from the pointer
		{
			if(senser[i] < min[i])
				min[i] = senser[i];
			else if(senser[i] > max[i])
				max[i] = senser[i];
		}
	}
	return 0;
}


//  This function normolize the values, as the raw data is to dynamci for each run. this function makes the values relarble 
//****************************************************************************************
void normel_lise(uint16_t *input, uint16_t *output, uint16_t *min, uint16_t *max, uint16_t rang_min , uint16_t rang_max, uint16_t antal)
{
  for (int i = 0; i < antal; i++)
  {
    output[i] = map(input[i], min[i], max[i], rang_min, rang_max);
  }
}

void setup()
{
  Serial.begin(9600);
  lineSensors.initFiveSensors();
  lcd.clear();
  kalibar(lineSensorValues, minlineSensorValues, maxlineSensorValues);         // Calling the kalibarstion function
  lcd.clear();
  for (int i = 0; i < NUM_SENSORS; i++)
  {
    Serial.println(i);
    Serial.print("max:");
    Serial.println(maxlineSensorValues[i]);
    Serial.print("min:");
    Serial.println(minlineSensorValues[i]);
    Serial.println("----------------");
  }
  while (digitalRead(buttonA))
  {}
}
void loop()
{
  lineSensors.read(lineSensorValues, useEmitters ? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);  // Reads the line sensors values
  normel_lise(lineSensorValues, output, minlineSensorValues, maxlineSensorValues, 0, 2000, NUM_SENSORS); // Calls the normelize function 

// Prints raw and normelized values from the output array
//******************************************
  for (int i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print("norm:");
    Serial.println(output[i]);
  }
  for (int i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print("raa:");
    Serial.println(lineSensorValues[i]);
  }
  Serial.println("---------------");
  delay(100);
}
