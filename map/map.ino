#include <Wire.h>
#include <Zumo32U4.h>
#define NUM_SENSORS 5
#define NUM_SENSERS_AREYS 15 

Zumo32U4LineSensors lineSensors; // instance of the sensor
Zumo32U4LCD lcd; 
Zumo32U4Motors motors;

uint16_t lineSensorValues[NUM_SENSERS_AREYS][NUM_SENSORS];
uint16_t maxlineSensorValues[NUM_SENSORS];
uint16_t minlineSensorValues[NUM_SENSORS];
uint16_t genlineSensorValues[NUM_SENSORS];
int buttonA = 14;
int line_sens_count = 0;
int8_t spede = 100;
int8_t l_speed = 150;
int8_t r_speed = -150;
bool ser_print = false;
bool vinkel = true; 

int kalibar_arveds(uint16_t *senser, uint16_t *min, uint16_t *max, bool qtr_emitters)
{
	//Initialize sensor values min and Max so they don't get preset from the initial values
	for(int i = 0; i < NUM_SENSORS; i++)
	{
		min[i] = genlineSensorValues[i];
		max[i] = genlineSensorValues[i];
	}
	while(digitalRead(buttonA))
	{
		linesens_matrix_update(&lineSensors, (uint16_t *)lineSensorValues, &line_sens_count, NUM_SENSERS_AREYS,NUM_SENSORS,qtr_emitters);
		//print_list(&(lineSensorValues[line_sens_count][0]));
		average_line_senser((uint16_t*)lineSensorValues, genlineSensorValues ,NUM_SENSERS_AREYS,NUM_SENSORS);
		if(ser_print)
			print_list(genlineSensorValues);
		for(int i = 0;i< NUM_SENSORS;i++)
		{
			if(genlineSensorValues[i] < min[i])
				min[i] = genlineSensorValues[i];
			else if(genlineSensorValues[i] > max[i])
				max[i] = genlineSensorValues[i];
		}
	}
	return 0;
}

void normel_lise(uint16_t *input,uint16_t *output, uint16_t *min, uint16_t *max, uint16_t rang_min , uint16_t rang_max, uint16_t antal)
{
	for(int i = 0;i < antal; i++)
	{
		output[i] = map(input[i],min[i],max[i],rang_min,rang_max);
	}
}

void average_line_senser(int *matrix, int *output, int areycount, int sensercount)
{
	long h = 0;
	for(int i = 0; i < sensercount; i++)
	{
		h = 0;
		for(int i2 = 0; i2 < areycount && (matrix + i2*sensercount)[i] != '\0' ; i2++ ) //There's a making additional 0 bity by check in the full loop so it ensures that the stop bite isn't overwritten
			h +=  (matrix + i2*sensercount)[i];
		output[i] =  h/areycount;
	}
}


void linesens_matrix_update(Zumo32U4LineSensors *li_sens, int *matrix , int *cound, int areycount, int sensercount,bool qtr_emitters )
{
	li_sens->read(( matrix + (*cound) * sensercount), qtr_emitters ? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);
	(*cound)++;	
	if(*cound >= areycount)
		*cound = 0;
}

//Check if any value in the given array is under the threshold
bool all_over_freshol(int *list,int threshol,int length )
{
	bool h = true;
	for(int i = 0; i < length ; i++)
	{
		h = h && (list[i] < threshol);
	}
	return h;
}

//Print a specified array in serial plotter format
void print_list(int *list)
{
	String h[NUM_SENSORS] = {"r:","rc:","c:","cl:","l:"};
	for(int i = 0; i < NUM_SENSORS;i++)
	{
		Serial.print(h[i]);
		Serial.print(list[i % NUM_SENSORS]);
		Serial.print("  ");
	}
	Serial.println("uT");
}

void AdjustToLine(int white)//Uses linesensors to detect a line, and turns the robot to be perpendicular on the detected line. Direction is determined by which sensor is first to detect the line.
//Code runs until the robot detects a line with all 3 sensors, indicating that it has adjusted to the line.
{
	Serial.println("AdjustToLine");
	int h = -100;
	int l = 0;
	do
	{
		linesens_matrix_update(&lineSensors, (uint16_t *)lineSensorValues, &line_sens_count, NUM_SENSERS_AREYS,NUM_SENSORS,true);
		average_line_senser((uint16_t*)lineSensorValues, genlineSensorValues ,NUM_SENSERS_AREYS,NUM_SENSORS);
		normel_lise(genlineSensorValues, genlineSensorValues, minlineSensorValues , maxlineSensorValues , 0 , 10000 , NUM_SENSORS);
		if(genlineSensorValues[0] < white)
		{
			Serial.println("RightRotation");
			motors.setSpeeds(h,l);
		}
		else if (genlineSensorValues[4] < white)
		{
			Serial.println("LeftRotation");
			motors.setSpeeds(l,h);
		}
		else
		{
			Serial.println("DriveForward");
			motors.setSpeeds(h,h);
		}
		print_list(genlineSensorValues);
	} while ((genlineSensorValues[0] > white && genlineSensorValues[2] > white && genlineSensorValues[4] > white) != true);
	motors.setSpeeds(0 , 0);
}

void until_sens(int whit)
{
	for(int i = 0; i < NUM_SENSERS_AREYS; i++)
	{
		linesens_matrix_update(&lineSensors, (uint16_t *)lineSensorValues, &line_sens_count, NUM_SENSERS_AREYS,NUM_SENSORS,true);
		average_line_senser((uint16_t*)lineSensorValues, genlineSensorValues ,NUM_SENSERS_AREYS,NUM_SENSORS);
	}
	do
	{
	linesens_matrix_update(&lineSensors, (uint16_t *)lineSensorValues, &line_sens_count, NUM_SENSERS_AREYS,NUM_SENSORS,true);
	average_line_senser((uint16_t*)lineSensorValues, genlineSensorValues ,NUM_SENSERS_AREYS,NUM_SENSORS);
	normel_lise(genlineSensorValues, genlineSensorValues, minlineSensorValues , maxlineSensorValues , 0 , 10000 , NUM_SENSORS);
	if(ser_print)
		print_list(genlineSensorValues);
	} while(all_over_freshol(genlineSensorValues, whit, NUM_SENSORS ));
}

void setup()
{
	Serial.begin(9600);
	lineSensors.initFiveSensors();
	if(ser_print)
		Serial.println("tryk paa kanp for at stop");
	lcd.print("tryk på a");
	kalibar_arveds((int*)lineSensorValues,minlineSensorValues,maxlineSensorValues, true);
	for(int i = 0; i < NUM_SENSORS && ser_print; i++)
	{
		Serial.println(i);
		Serial.print("max:");
		Serial.println(maxlineSensorValues[i]);
		Serial.print("min:");
		Serial.println(minlineSensorValues[i]);
		Serial.println("----------------");  
	}
}

void loop()
{
	lcd.clear();
	motors.setSpeeds(spede,spede);
	delay(100);
	until_sens(8000);
	motors.setSpeeds(-spede, -spede);
	delay(100);
	motors.setSpeeds(l_speed, r_speed );
	delay(1000);
	motors.setSpeeds(spede, spede);
	delay(600);
	motors.setSpeeds(l_speed, r_speed );
	delay(1000);
	motors.setSpeeds(-spede, -spede);
	until_sens(8000);
	AdjustToLine(8000);
	motors.setSpeeds(0, 0);
	l_speed = l_speed * -1;
	r_speed = r_speed * -1;
}
